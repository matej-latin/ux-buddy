---
layout: barebones
title: You’re in!
noindex: true
custom-js:
  - /assets/js/confetti.js
body-class: thanks
---
Yaaaaaay! 🎉 Thanks so much for joining the waitlist! UX Buddy registrations are opening again soon and you’ll be among the first to know. You’ll also get the <span class="thanks-discount">early bird</span> discount!

<!-- <h2 class="h5 thanks-referral-title">Share with your friends, get extra 50% off!</h2>

<p class="thanks-referral-text">
  Btw, I’d love it if you could share UX Buddy with other designers looking to make the next steps in their UX careers. If one of your friends signs up, you both get an additional 50% discount. So you and your friend would get the course for a 75% total discounted price—that’s $50 instead of $195! 😲
</p>

I’ll send you the instructions on how to refer your friends so check your inbox.

<small>This offer is only available for a limited time, until UX Buddy launches.</small> -->

<!-- <a id="email-share" target="_blank" class="button-cta-large" href="mailto:your.friends@email.com?subject=Hey%2C%20check%20this%20out!&body=I%20just%20signed%20up%20for%20this%20course%2C%20it%20says%20it%E2%80%99s%20coming%20soon%20and%20you%20can%20still%20get%20a%20huge%20discount%3A%20https%3A%2F%2Fuxbuddy.co">Share via email →</a> -->
