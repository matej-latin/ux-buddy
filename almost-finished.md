---
layout: barebones
title: Almost finished
noindex: true
---

I just need to confirm your email so please check your inbox and click on the email confirmation link that was just sent to you.

🤫 Psst! Sometimes these emails land in Promo or even Spam folders so if you can’t see the email, check those too.

[← Go back to the homepage](/)