// Capitalize string
var capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}

// Set cookie function
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

// Get cookie function
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

// // Check referral cookie function
// function checkReferralCookie() {
//   var referral = getCookie("referral");
//   if (referral != "") {
//     return referral;
//   } else {
//     return null;
//   }
// }

// Check existing cookie function
function checkExistingCookie() {
    var existing = getCookie("existing");
    if (existing != "") {
        $('#EXISTING').val(existing);

        var name = getCookie("name");
        $('#FNAME').val(name);
        var surname = getCookie("surname");
        $('#LNAME').val(surname);
        var email = getCookie("email");
        $('#EMAIL').val(email);

        $('.first-name').html(', ' + capitalize(name));
        $('.msg-alt').html('check and submit');
    }
}

// Get data from URL
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

var varEmail = getParameterByName('email');
var varName = getParameterByName('name');
var varSurname = getParameterByName('surname');
var varReferral = getParameterByName('r');

// // Is user referred?
// if (varReferral != null) {
//   $('#REFERRAL').val(varReferral);
//   setCookie("referral", varReferral, 365);
//   $('.buy-option-price.early').html('<del class="f-cl-body-black">$95</del> $50');
// } else {
//   var referral = checkReferralCookie();
//   if (referral != null) {
//     $('#REFERRAL').val(referral);
//     $('.buy-option-price.early').html('<del class="f-cl-body-black">$95</del> $50');
//   }
// }

// Is user an existing user?
if (varName != null) {

    // Prefill form
    $('#FNAME').val(capitalize(varName));
    setCookie("name", capitalize(varName), 365);
    $('#LNAME').val(capitalize(varSurname));
    setCookie("surname", capitalize(varSurname), 365);
    $('#EMAIL').val(varEmail);
    setCookie("email", varEmail, 365);

    // Indicate existing user
    $('#EXISTING').val('Yes');
    setCookie("existing", "Yes", 365);

    // Add name to signup message
    $('.first-name').html(', ' + capitalize(varName));
    $('.msg-alt').html('check and submit');

} else {
    checkExistingCookie();
}

// TAWK.TO
// if (varName != null) {
//   if (varSurname != null) {
//     var fullName = capitalize(varName) + ' ' + capitalize(varSurname);
//   } else {
//     var fullName = capitalize(varName);
//   }

//   var Tawk_API = Tawk_API || {};
//   Tawk_API.visitor = {
//     name: fullName,
//     email: varEmail
//   };

//   var Tawk_LoadStart = new Date();
// }

// var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
// (function () {
//   var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
//   s1.async = true;
//   s1.src = 'https://embed.tawk.to/5d5a5e1e77aa790be32f9139/default';
//   s1.charset = 'UTF-8';
//   s1.setAttribute('crossorigin', '*');
//   s0.parentNode.insertBefore(s1, s0);
// })();

// HOTJAR
// (function (h, o, t, j, a, r) {
//   h.hj = h.hj || function () { (h.hj.q = h.hj.q || []).push(arguments) };
//   h._hjSettings = { hjid: 1449607, hjsv: 6 };
//   a = o.getElementsByTagName('head')[0];
//   r = o.createElement('script'); r.async = 1;
//   r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
//   a.appendChild(r);
// })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');

// Scroll to anchor
$(function () {
    $('a[href*=#]:not([href=#])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            // console.log(target);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // console.log(target);
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 800);
                return false;
            }
        }
    });
});

function animateOnScroll(element, animation, triggerDelay, trigger) {
    if (trigger == null) {
        trigger = element;
    }

    // var triggerPoint = 0;
    var triggerPoint;
    var scrollPosition;

    $(window).on('load', function () {
        triggerPoint = $(trigger).offset().top;
    });

    $(window).on('scroll', function () {
        scrollPosition = window.pageYOffset + window.innerHeight;
        // console.log(scrollPosition);
        var calculateDelay = triggerDelay + 's';

        if (scrollPosition > triggerPoint) {
            $(element).css("animation-delay", calculateDelay).removeClass('o-0').addClass(animation);
        }
    });
}

// Animate children on scroll
function animateChildrenOnScroll(parent, animation, delay, triggerDelay, trigger) {
    if (triggerDelay == null) {
        triggerDelay = 0
    }
    if (trigger == null) {
        trigger = parent;
    }
    if (delay == null) {
        delay = 0;
    }
    var triggerPoint;
    var scrollPosition;
    var children = $(parent).children();

    $(window).on('load', function () {
        triggerPoint = $(trigger).offset().top;
    });

    $(window).on('scroll', function () {
        scrollPosition = window.pageYOffset + window.innerHeight;

        if (scrollPosition > triggerPoint) {
            for (i = 0; i < children.length; i++) {
                var calculateDelay = triggerDelay + (i * delay) + 's';

                $(children[i]).css("animation-delay", calculateDelay).removeClass('o-0').addClass(animation);
            }
        }
    });
}

// Thanks page
// $(function () {
//   if ($('body').is('.thanks')) {
//     referral = checkReferralCookie();

//     if (referral.length > 0) {
//       $('.thanks-discount').html('75%');
//       $('.thanks-referral-title').html('Give your designer friends the 75% discount&nbsp;too!');
//       $('.thanks-referral-text').html('Do you know any other designers that are thinking about taking the next steps in their UX careers? If you refer them, they’ll also get the extra 50% discount, resulting in a total of 75% discount—that’s $50 instead of $195! 😲');
//     }
//   }
// });

// If page has form
$(function () {
    if ($('body').is('.hasForm')) {
        // Button spinner behavior
        var myButton = document.getElementById('myButton');

        // Submit button event + animations
        myButton.addEventListener('click', function () {
            if ($('#FNAME').val().length === 0 || $('#LNAME').val().length === 0 || $('#EMAIL').val().length === 0) {
                $('#myButton').addClass('loading');
                $('#myButton').html('Joining...');
                setTimeout(function () {
                    $('#myButton').removeClass('loading');
                    $('#myButton').addClass('shake');
                    $('#myButton').html('Join the waitlist');
                    setTimeout(function () {
                        $('#myButton').removeClass('shake');
                    }, 1000);
                }, 400);
            } else {
                $('#myButton').addClass('loading');
                $('#myButton').html('Joining...');
            }
        }, false);

        (function (window, document) {
            'use strict';

            $('.btnJoin').on('click', function () {
                $('.sign-up-form-wrap').fadeIn();
            });

            $('.sign-up-form svg').on('click', function () {
                $('.sign-up-form-wrap').fadeOut();
            });

            // On form submit
            // $('form.sign-up').on('submit', function (e) {
            //   // Hijack the submit button, we will do it ourselves
            //   e.preventDefault();

            //   var check = $('#CHECK').val();

            //   if ( check.length > 0 ) {
            //     return;
            //   }

            //   // store all the form data in a variable
            //   var formData = $(this).serialize();

            //   $.ajax({
            //     url: '//course.uxbuddy.co/endpoints/uxb/',
            //     data: formData,
            //     dataType: 'json',

            //     error: function (err) {
            //       $('#msgContent').html('<h5>Oops!</h5><p>Something went wrong, please try again later. If the problem persists, try <a href="http://eepurl.com/gBk8wH">signing up here</a>.</p>');

            //       setTimeout(function () {
            //         $('#myButton').removeClass('loading');
            //         $('#myButton').addClass('shake');
            //         $('#myButton').html('Join the waitlist');
            //         setTimeout(function () {
            //           $('#myButton').removeClass('shake');
            //         }, 1000);
            //       }, 400);

            //       setTimeout(function () {
            //         $('#msg').fadeIn(300);
            //         $('.sign-up-form').animate({
            //           scrollTop: $('#msgContent').offset().top
            //         }, 500);
            //       }, 1000);

            //     },

            //     success: function (data) {
            //       if (data.status === "subscribed") {
            //         // SUCCESS
            //         window.location.href = '/thanks/';

            //       } else {
            //         // ERROR
            //         console.log(data);
            //         $('#msgContent').html('<h5>Oops!</h5><p>Something went wrong, please try again later. If the problem persists, try <a href="http://eepurl.com/gBk8wH">signing up here</a>.</p>');

            //         setTimeout(function () {
            //           $('#myButton').removeClass('loading');
            //           $('#myButton').addClass('shake');
            //           $('#myButton').html('Join the waitlist');
            //           setTimeout(function () {
            //             $('#myButton').removeClass('shake');
            //           }, 1000);
            //         }, 400);

            //         setTimeout(function () {
            //           $('#msg').fadeIn(300);
            //           $('.sign-up-form').animate({
            //             scrollTop: $('#msgContent').offset().top
            //           }, 500);
            //         }, 1000);
            //       }
            //     }

            //   });

            //   return false;

            // });

        }(window, document));

    } // End of if statement
}); // End of form stuff

// GA events
$(function () {
    $('.btnJoin').on('click', function () {
        gtag('event', 'Join the waitlist 1', {
            'event_category': 'CTA'
        });
    });

    $('#myButton').on('click', function () {
        gtag('event', 'Join the waitlist 2', {
            'event_category': 'CTA'
        });
    });

    $('#link-about-author').on('click', function () {
        gtag('event', 'About Author', {
            'event_category': 'Navigation'
        });
    });

    $('#link-join-waitlist').on('click', function () {
        gtag('event', 'Join the waitlist (nav)', {
            'event_category': 'Navigation'
        });
    });

    $('#email-share').on('click', function () {
        gtag('event', 'Email share', {
            'event_category': 'CTA'
        });
    });

    $('.accordion-tab-label').on('click', function (e) {
        var target = e.currentTarget.outerText;

        gtag('event', target, {
            'event_category': 'FAQs'
        });
    });

    $('.toc-step a').on('click', function (e) {
        var target = e.currentTarget.outerText;

        gtag('event', target, {
            'event_category': 'Five steps TOC'
        });
    });

});

// Homepage stuff
$(function () {
    if ($('article').is('.home')) {
        animateChildrenOnScroll('.solution-statement h2', 'fadeIn', 1, 0.3);
        animateOnScroll('.charge-explanation', 'fadeIn', 1);

        // 5 steps toc on click
        $(function () {
            $('.five-steps-toc a').on('click', function () {
                $('.toc-step').removeClass('active');
                // $(this).parent().addClass('active');
            });
        });

        // 5 steps toc on scroll
        $(function () {
            var scrollPosition;
            var triggerPoints = [];

            $(window).on('load', function () {
                var i;
                for (i = 0; i < 5; i++) {
                    let step = '#step-' + (i + 1);
                    triggerPoints.push($(step).offset().top);
                }
                // console.log(triggerPoints);
            });

            $(window).on('scroll', function () {
                scrollPosition = window.pageYOffset + window.innerHeight;
                // console.log(scrollPosition);
                var i;
                for (i = 0; i < triggerPoints.length; i++) {
                    if (scrollPosition > triggerPoints[i]) {
                        let step = '.step-' + (i + 1);
                        $('.toc-step').removeClass('active');
                        $(step).addClass('active');
                    }
                }
            });
        });

        // Countdown timer
        // Set the date we're counting down to
        var countDownDate = new Date("Mar 11, 2024 7:59:00").getTime();

        // Update the count down every 1 second
        var x = setInterval(function () {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.ceil(distance / (1000 * 60 * 60 * 24));

            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            //   console.log(days, hours);
            // Display the result in the element with id="demo"
            if (days > 2) {
                // document.getElementById("timer-m").innerHTML = "in " + days + " days";
                document.getElementById("timer-c").innerHTML = "in " + days + " days";
            } else if (days == 1 && hours > 0) {
                // document.getElementById("timer-m").innerHTML = "in " + days + " day " + hours + " hours";
                document.getElementById("timer-c").innerHTML = "in " + days + " day " + hours + " hours";
            } else if (days == 1 && hours == 0) {
                // document.getElementById("timer-m").innerHTML = "today";
                document.getElementById("timer-c").innerHTML = "today";
            } else if (days < 1) {
                // document.getElementById("timer-m").innerHTML = "in " + hours + " hours";
                document.getElementById("timer-c").innerHTML = "in " + hours + " hours";
            } else {
                // document.getElementById("timer-m").innerHTML = "in " + days + " days";
                document.getElementById("timer-c").innerHTML = "in " + days + " days";
            }

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                // document.getElementById("timer-m").innerHTML = "today";
                document.getElementById("timer-c").innerHTML = "today";
            }
        }, 1000);

    }
});
