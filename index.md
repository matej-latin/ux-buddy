---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
body-class: hasForm
image: https://uxbuddy.co/assets/img/social.jpg?4362984388
title: Get an awesome UX design job
custom-js: 
  - https://f.convertkit.com/ckjs/ck.5.js 
---
